#!/usr/bin/python
# coding: utf-8
import json
import os
import shutil
import tempfile
import collections
import unittest

from webtest import TestApp, Upload

from fuxico_backend.sementeira import app


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.client = TestApp(app)
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_dir)


class HelloTest(BaseTest):
    def test_basic_endpoint_ok(self):
        # Basic Test Endpoint
        result = self.client.get("/hello")
        self.assertEqual(result.status_code, 200)


class DeleteTest(BaseTest):
    def setUp(self):
        super().setUp()
        self.test_path = os.path.join(self.test_dir, "test.png")
        with open(self.test_path, "w+b") as fp:
            fp.write(b'Teste123456')

    def test_dir_not_set(self):
        del os.environ["UPLOADFOLDER"]
        expected = {
            "status_code": 404,
            "message": 'Could not delete shared file "/test.png": No upload directory set.'
        }
        # /<filepath>
        result = self.client.delete("/test.png", expect_errors=True)
        self.assertEqual(result.status_int, 404)
        self.assertEqual(result.body, json.dumps(expected).encode())
        self.assertTrue(os.path.exists(self.test_path))

    def test_file_not_found(self):
        os.environ["UPLOADFOLDER"] = self.test_dir
        # /<filepath>
        result = self.client.delete("/test.txt")
        self.assertEqual(result.status_int, 204)

    def test_delete_file_ok(self):
        os.environ["UPLOADFOLDER"] = self.test_dir
        # /<filepath>
        result = self.client.delete("/test.png")
        self.assertEqual(result.status_int, 204)
        self.assertFalse(os.path.exists(self.test_path))


class UploadTest(BaseTest):
    def test_no_file_sent(self):
        expected = {
            "status_code": 400,
            "message": 'upload field is mandatory.'
        }
        # /Shared
        result = self.client.post("/upload/", expect_errors=True)
        self.assertEqual(result.status_int, 400)
        self.assertEqual(result.body, json.dumps(expected).encode())

    def test_dir_not_set(self):
        expected = {
            "status_code": 404,
            "message": 'Could not save uploaded file "/Shared/test.txt": No upload directory set.'
        }
        files = {
            'upload': Upload('test.txt', b'Teste123456', 'text/plain')
        }
        result = self.client.post("/upload/", params=files, expect_errors=True)
        self.assertEqual(result.status_int, 404)
        self.assertEqual(result.body, json.dumps(expected).encode())
        self.assertFalse(os.path.exists(os.path.join(self.test_dir, 'test.txt')))

    def test_upload_file_ok(self):
        os.environ["UPLOADFOLDER"] = self.test_dir
        files = {
            'upload': Upload('test.txt', b'Teste123456', 'text/plain')
        }
        result = self.client.post("/upload/", params=files)
        self.assertEqual(result.status_int, 201)
        self.assertTrue(os.path.exists(os.path.join(self.test_dir, 'test.txt')))


class SharedTest(BaseTest):
    def test_sementeira_upload_dir_not_set(self):
        expected = {
            "status_code": 404,
            "message": 'Could not get shared file "/Shared/": No upload directory set.'
        }
        # /Shared
        result = self.client.get("/Shared", expect_errors=True)
        self.assertEqual(result.status_int, 404)
        self.assertEqual(result.body, json.dumps(expected).encode())
        result = self.client.get("/Shared/", expect_errors=True)
        self.assertEqual(result.status_int, 404)

    def test_sementeira_root_ok(self):
        # Create a temporary directory
        os.environ["UPLOADFOLDER"] = self.test_dir
        fp = open(os.path.join(self.test_dir, "test.png"), "w+b")
        # /Shared
        result = self.client.get("/Shared")
        self.assertEqual(result.status_code, 200)
        result = self.client.get("/Shared/")
        self.assertEqual(result.status_code, 200)
        # Remove the directory after the test
        fp.close()
        del os.environ["UPLOADFOLDER"]

    def test_sementeira_root_returns_file_list(self):
        # Create a temporary directory
        os.environ["UPLOADFOLDER"] = self.test_dir
        fp = open(os.path.join(self.test_dir, "test.png"), "w+b")
        # /Shared
        result = self.client.get("/Shared")
        self.assertEqual(result.body, b'["test.png"]')
        result = self.client.get("/Shared/")
        self.assertEqual(result.body, b'["test.png"]')
        # Remove the directory after the test
        fp.close()
        del os.environ["UPLOADFOLDER"]


class RenameTest(BaseTest):
    def test_upload_dir_not_set(self):
        expected = {
            "status_code": 404,
            "message": 'Could not rename file "test.png": No upload directory set.'
        }
        # /rename/<filepath>
        result = self.client.patch(
            "/rename/test.png", {"new_filename": "new_test.png"}, expect_errors=True
        )
        self.assertEqual(result.status_int, 404)
        self.assertEqual(result.body, json.dumps(expected).encode())

    def test_file_not_found(self):
        os.environ["UPLOADFOLDER"] = self.test_dir
        expected = {
            "status_code": 404,
            "message": 'Could not rename file "test.png": No such file or directory.'
        }
        # /rename/<filepath>
        result = self.client.patch(
            "/rename/test.png", {"new_filename": "new_test.png"}, expect_errors=True
        )
        self.assertEqual(result.status_int, 404)
        self.assertEqual(result.body, json.dumps(expected).encode())

    def test_file_renamed_ok(self):
        os.environ["UPLOADFOLDER"] = self.test_dir
        with open(os.path.join(self.test_dir, "test.png"), "w+b"):
            # /rename/<filepath>
            result = self.client.patch(
                "/rename/test.png", {"new_filename": "new_test.png"}
            )
            self.assertEqual(result.status_int, 204)


class DiskUsageTest(BaseTest):
    def test_sementeira_dir_not_set(self):
        expected = {
            "status_code": 404,
            "message": "Could not get disk usage: No upload directory set."
        }
        # /disk-usage
        result = self.client.get("/disk-usage", expect_errors=True)
        self.assertEqual(result.status_int, 404)
        self.assertEqual(result.body, json.dumps(expected).encode())
        # /disk-usage/
        result = self.client.get("/disk-usage/", expect_errors=True)
        self.assertEqual(result.status_int, 404)

    def test_disk_usage_ok(self):
        os.environ["UPLOADFOLDER"] = self.test_dir
        # /disk-usage/
        result = self.client.get("/disk-usage")
        self.assertEqual(result.status_int, 200)
        content = json.loads(result.body.decode("utf8"))
        self.assertIn("total", content.keys())
        self.assertIn("used", content.keys())
        self.assertIn("free", content.keys())


class InstallTest(BaseTest):
    def test_bad_request(self):
        expected = {
            "status_code": 400,
            "message": "Could not install Fuxico:"
            " mandatory params: ['sementeira', 'sysKey', 'storage']"
        }
        # /disk-usage
        result = self.client.post(
            "/install/setup", collections.OrderedDict([]), expect_errors=True)
        self.assertEqual(result.status_int, 400)
        self.assertEqual(result.body, json.dumps(expected).encode())
        # /disk-usage/
        result = self.client.post(
            "/install/setup/", collections.OrderedDict([]), expect_errors=True)
        self.assertEqual(result.status_int, 400)


if __name__ == "__main__":
    unittest.main()
