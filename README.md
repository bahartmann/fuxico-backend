# Fuxico Backend

Backend que manipula os arquivos da Fuxico. Lista, lê, salva e deleta arquivos, com os seguintes endpoints:

## Interface REST
*  POST `/<diretório compartilhado>/upload/`: salva arquivo
*  GET `/<diretório compartilhado>/`: lista arquivos no diretório da sementeira
*  GET `/<diretório compartilhado>/<nome do diretório>`: lista arquivos no diretório dentro da sementeira
*  GET `/<diretório compartilhado>/<nome do arquivo>`: lê arquivo
*  DELETE `/<path do arquivo>`: deleta arquivo
*  PATCH `/<path do arquivo>`: renomeia arquivo
*  POST `/install/setup`: instalação

## Instalação
- Baixe o repositório
- Crie uma virtualenv python com o comando `python -m venv fuxico-backend` e ative-a com o comando `source fuxico-backend/bin/activate`
- Execute o comando `python setup.py install` para instalar a aplicação

## Testes Automatizados
- Execute o comando `python setup.py test`

## Para rodar a Sementeira
- Defina a variável de ambiente `UPLOADFOLDER` com path do diretório onde os arquivos da Sementeira serão armazenados
- Execute o comando `bottle.py -b 0.0.0.0:8080 fuxico_backend.sementeira:app` para colocar a Sementeira no ar
